//
//  main.m
//  BPM Inspector
//
//  Created by Administrator on 8/28/17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
