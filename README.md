# BPM Inspector #
This is a remake/clone/workalike of the old iTunes-BPM utility from BlackTree, which apart from hard to find these days works only on PPC machines or systems that support and include Rosetta (maximum of MacOS X 10.6.8). It enables you to tap along with a song in iTunes to find its BPM, which you can then set for the selected song(s). The source code is available under an MIT license.

Ready to use binaries for 10.4 and up are available from the downloads section.

Instruction for installing binary
---------------------------------
- Place the application in /Users/[your user name]/Library/iTunes/Scripts (if the directory 'Scripts' does not exist yet, create it)
- Launch the application once; it will now copy a script to the folder in which it resides
- You can now select 'BPM Inspector' from the Scripts menu in iTunes whenever you need it.


Building
--------
Project is for Xcode 3.1 (binary was built on MacOS X 10.6.8 Server). Currently set to build for 32-bit/64-bit for 10.4 and up. Tested on 10.6.8 and 10.13.1. Will build without problems in Xcode 9.

Current version is 1.01 (build 16).