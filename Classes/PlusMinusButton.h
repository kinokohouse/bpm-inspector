//
//  PlusMinusButton.h
//  BPM Inspector
//
//  Created by Petros Loukareas on 27-08-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#ifndef PlusMinusButton_h
#define PlusMinusButton_h


#endif /* PlusMinusButton_h */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"


@interface PlusMinusButton : NSButton {
    
    NSTimer *repeatTimer;
    NSUInteger buttonPosition;
    
}

#pragma mark Methods
- (void)mouseUp:(NSEvent *)event;
- (void)mouseDown:(NSEvent *)event;
- (void)setFirstTimer;
- (void)setNextTimer;

@end
