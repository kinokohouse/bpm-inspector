//
//  AppDelegate.m
//  BPM Inspector
//
//  Created by Petros Loukareas on 26-08-17.
//  Last updated on 25-11-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "AppDelegate.h"


@implementation AppDelegate


#pragma mark Getters and Setters

- (unsigned int)beatsPerMinute {
    return beatsPerMinute;
}

- (void)setBeatsPerMinute:(unsigned int)value {
    beatsPerMinute = value;
    timeInterval = 60.0 / beatsPerMinute;
    [bpmMeter setStringValue: [NSString stringWithFormat:@"%lu bpm", beatsPerMinute]];
    lastInterval = 0;
    [self initBlinkTimer:timeInterval];
}

- (unsigned int)OSVersion {
    return OSVersion;
}


#pragma mark Application Delegate Methods

- (BOOL)application:(NSApplication *)sender delegateHandlesKey:(NSString *)key {
    if ([key isEqual: @"timeInterval"] || [key isEqual: @"beatsPerMinute"]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    timeInterval = 1;
    maxTimeList = 16;
    currentAverage = 0.0;
    beatsPerMinute = 60;
    precisionSize = 0.05;
    [timeList = [NSMutableArray arrayWithCapacity:maxTimeList] retain];
    theColorTable = [[NSMutableArray alloc] init];
    picToggle = NO;
    blinkTimer = nil;
    delayTimer = nil;
    regDrumImage = [NSImage imageNamed:@"left-diffuse.png"];
    altDrumImage = [NSImage imageNamed:@"right-diffuse.png"];
    lampOnImage = [NSImage imageNamed:@"lamp-on.png"];
    lampOffImage = [NSImage imageNamed:@"lamp-off.png"];
    [theBigButton sendActionOn:NSLeftMouseDownMask];
    [self getOSVersion];
    if (OSVersion > 9) {
        [closeBtn setImage: [NSImage imageNamed:@"modern.png"]];
        [closeBtn setAlternateImage: [NSImage imageNamed:@"modern-mousedown.png"]];
        [fakeMinButton setImage: [NSImage imageNamed:@"modern-open.png"]];
        [fakeZoomButton setImage: [NSImage imageNamed:@"modern-open.png"]];
    }
    [self buildColorTable];
    [self copyResourcesIfNeeded];
    [self getDefaults];
    [self initBlinkTimer:timeInterval];
    [theWindow setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"window-backdrop.png"]]];
    [theWindow setLevel:NSFloatingWindowLevel];
    NSRect myBounds = [closeBtn bounds];
    uint8_t myOptions = NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways;
    NSTrackingArea *myTrackingArea = [[NSTrackingArea alloc] initWithRect:myBounds options:myOptions owner:closeBtn userInfo:nil];
    [closeBtn addTrackingArea:myTrackingArea];
    [myTrackingArea release];
    [[NSWorkspace sharedWorkspace] launchApplication:@"iTunes"];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    [self setDefaults];
    return NSTerminateNow;
}


#pragma mark IBActions

- (IBAction)gotClicked:(NSApplication *)sender {
    double myInterval = [NSDate timeIntervalSinceReferenceDate];
    if (lastInterval == 0) {
        [self changeButtonPicOnly];
        [timeList removeAllObjects];
        lastInterval = myInterval;
        colorIndex = 0;
        [self initDelayTimer:1.0];
    } else {
        if (blinkTimer != nil) {
            [self stopBlinkTimer];
        }
        if (delayTimer != nil) {
            [self stopDelayTimer];
        }
        [self changeButtonPicOnly];
        double trueInterval = myInterval - lastInterval;
        [timeList addObject:[NSNumber numberWithFloat:trueInterval]];
        if ([timeList count] > maxTimeList) {
            [timeList removeObjectsInRange:NSMakeRange(0,1)];
        }
        double av = 0.0;
        for(int i = 0; i < [timeList count]; i++) {
            double currentValue = [(NSNumber *)[timeList objectAtIndex:i] floatValue];
            av = av + currentValue;
        }
        currentAverage = av / [timeList count];
        beatsPerMinute = 60.0 / currentAverage;
        double myPrecisionSize = precisionSize;
        if ((trueInterval > 0.2) && (trueInterval < 0.3)) {
            myPrecisionSize = myPrecisionSize * 1.5;
        }
        if (trueInterval <= 0.2) {
            myPrecisionSize = myPrecisionSize * 2.0;
        }
        double belowValue = currentAverage - (currentAverage * myPrecisionSize);
        double aboveValue = currentAverage + (currentAverage * myPrecisionSize);
        double lowerValue = currentAverage - (currentAverage * (myPrecisionSize * 2));
        double higherValue = currentAverage + (currentAverage * (myPrecisionSize * 2));
        double evenLowerValue = currentAverage - (currentAverage * (myPrecisionSize * 4));
        double evenHigherValue = currentAverage + (currentAverage * (myPrecisionSize * 4));
        if ((belowValue < trueInterval) && (trueInterval < aboveValue)) {
            if (colorIndex < 15) {
                colorIndex++;
            }
        }
        if (((lowerValue < trueInterval) && (trueInterval < belowValue)) || ((higherValue > trueInterval) && (trueInterval > aboveValue))) {
            if (colorIndex > 0) {
                colorIndex--;
            }
        }
        if ((trueInterval < lowerValue) || (trueInterval > higherValue)) {
            if (colorIndex > 1) {
                colorIndex = colorIndex - 2;
            }
        }
        if ((trueInterval < evenLowerValue) || (trueInterval > evenHigherValue)) {
            colorIndex = 0;
            unsigned long c = [timeList count];
            if (c > 2) {
                [timeList removeObjectsInRange: NSMakeRange(0, c - 2)];
            }
        }
        [bpmMeter setStringValue: [NSString stringWithFormat:@"%lu bpm", beatsPerMinute]];
        [bpmMeter setTextColor:[theColorTable objectAtIndex:colorIndex]];
        lastInterval = myInterval;
        [self initDelayTimer:trueInterval];
    }
}

- (IBAction)gotSet:(NSApplication *)sender {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.iTunes\"\nif selection is not {} then\nrepeat with i from 1 to (count of items in selection)\nset bpm of (item i of selection) to %lu\nend repeat\nelse\nbeep\nend if\nend tell\n", beatsPerMinute];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if ([returnValue data].length > 0) {
        NSLog(@"Problem setting BPM of selected tracks in iTunes; more info below:");
        NSLog(@"%@", errorDict);
    }
    [scriptObject release];
}

- (IBAction)quitTheApp:(NSApplication *)sender {
    if (timeList) {
        [timeList release];
    }
    if (theColorTable) {
        [theColorTable release];
    }
    [NSApp terminate: nil];
}


#pragma mark Application Methods

- (void)getDefaults {
    NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];
    NSPoint thePoint = [self getDefaultWindowPosition];
    long theX = thePoint.x;
    long theY = thePoint.y;
    NSDictionary *defaultPrefs = [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNumber numberWithLong:theX], @"floaterX",
                               [NSNumber numberWithLong:theY], @"floaterY",
                               nil];
    [myUserDefaults registerDefaults:defaultPrefs];
    long myX = [myUserDefaults integerForKey:@"floaterX"];
    long myY = [myUserDefaults integerForKey:@"floaterY"];
    [theWindow setFrameOrigin:NSMakePoint(myX, myY)];
}

- (void)setDefaults {
    NSPoint thePoint = [theWindow frame].origin;
    NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];
    [myUserDefaults setInteger: thePoint.x forKey: @"floaterX"];
    [myUserDefaults setInteger: thePoint.y forKey: @"floaterY"];
}

- (void)changeThePics {
    [self changeButtonPicOnly];
    if (picToggle == YES) {
        [leftLamp setImage:lampOnImage];
        [rightLamp setImage: lampOffImage];
        picToggle = NO;
    } else {
        [leftLamp setImage:lampOffImage];
        [rightLamp setImage: lampOnImage];
        picToggle = YES;
    }
}

- (void)changeButtonPicOnly {
    [leftLamp setImage:lampOffImage];
    [rightLamp setImage:lampOffImage];
    if ([theBigButton.image isEqual:altDrumImage]) {
        [theBigButton setImage: regDrumImage];
        picToggle = YES;
    } else {
        [theBigButton setImage: altDrumImage];
        picToggle = NO;
    }
}

- (void)increaseBPM {
    if (beatsPerMinute < 999) {
        beatsPerMinute++;
        [bpmMeter setStringValue: [NSString stringWithFormat:@"%lu bpm", beatsPerMinute]];
    }
}

- (void)decreaseBPM {
    if (beatsPerMinute > 1) {
        beatsPerMinute--;
        [bpmMeter setStringValue: [NSString stringWithFormat:@"%lu bpm", beatsPerMinute]];
    }
}

- (void)applyNewBPM {
    timeInterval = 60.0 / beatsPerMinute;
    [self initBlinkTimer:timeInterval];
}

- (void)buildColorTable {
    int i;
    NSArray *theTable = [NSArray arrayWithObjects: [NSArray arrayWithObjects: [NSNumber numberWithInt: 163], [NSNumber numberWithInt: 163], [NSNumber numberWithInt: 163], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 153], [NSNumber numberWithInt: 153], [NSNumber numberWithInt: 153], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 128], [NSNumber numberWithInt: 128], [NSNumber numberWithInt: 128], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 102], [NSNumber numberWithInt: 102], [NSNumber numberWithInt: 102], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 77], [NSNumber numberWithInt: 77], [NSNumber numberWithInt: 77], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 51], [NSNumber numberWithInt: 51], [NSNumber numberWithInt: 51], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 26], [NSNumber numberWithInt: 26], [NSNumber numberWithInt: 26], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 0], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 2], [NSNumber numberWithInt: 26], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 4], [NSNumber numberWithInt: 51], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 6], [NSNumber numberWithInt: 76], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 8], [NSNumber numberWithInt: 102], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 11], [NSNumber numberWithInt: 128], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 13], [NSNumber numberWithInt: 153], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 15], [NSNumber numberWithInt: 178], nil], [NSArray arrayWithObjects: [NSNumber numberWithInt: 0], [NSNumber numberWithInt: 17], [NSNumber numberWithInt: 204], nil], nil];
    unsigned long tableSize = [theTable count];
    for (i = 0; i < tableSize; i++) {
        NSArray *theColor = [theTable objectAtIndex:i];
        double theRed = [(NSNumber *) [theColor objectAtIndex:0] floatValue] / 256.0;
        double theGreen = [(NSNumber *) [theColor objectAtIndex:1] floatValue] / 256.0;
        double theBlue = [(NSNumber *) [theColor objectAtIndex:2] floatValue] / 256.0;
        NSColor *theNSColor = [NSColor colorWithCalibratedRed:theRed green:theGreen blue:theBlue alpha:1.0];
        [theColorTable addObject:theNSColor];
    }
}

- (void)initBlinkTimer:(NSTimeInterval)interval {
    if (interval == 0) {
        interval = timeInterval;
    }
    if (blinkTimer != nil) {
        [self stopBlinkTimer];
    }
    picToggle = NO;
    timeInterval = interval;
    blinkTimer = [[NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(changeThePics) userInfo:nil repeats:YES] retain];
}

- (void)initDelayTimer:(NSTimeInterval)interval {
    if (delayTimer != nil) {
        [self stopDelayTimer];
    }
    interval = interval * 2;
    if (interval > 2.5) {
        interval = 2.5;
    }
    delayTimer = [[NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(fireDelayTimer) userInfo:nil repeats:YES] retain];
}

- (void)stopBlinkTimer {
    if ([blinkTimer isValid]) {
        [blinkTimer invalidate];
        [blinkTimer release];
    }
    blinkTimer = nil;
}

- (void)stopDelayTimer {
    if ([delayTimer isValid]) {
        [delayTimer invalidate];
        [delayTimer release];
    }
    delayTimer = nil;
}

- (void)fireDelayTimer {
    [self stopDelayTimer];
    [self initBlinkTimer:currentAverage];
    lastInterval = 0;
}

- (void)copyResourcesIfNeeded {
    NSURL *URLToMe = [[NSBundle mainBundle] bundleURL];
    NSString *pathToMe = URLToMe.path;
    NSString *pathToCopy = [[NSString stringWithFormat:@"%@",pathToMe] stringByDeletingLastPathComponent];
    NSString *pathToCopyTo = [NSString stringWithFormat:@"%@%@",pathToCopy,@"/BPM Inspector.scpt"];
    if ([pathToCopyTo rangeOfString:@"iTunes"].location == NSNotFound || [pathToCopyTo rangeOfString:@"Scripts"].location == NSNotFound) {
        // NSLog(@"Application not in expected path - script not copied");
        return;
    }
    NSString *checkExistence = [NSString stringWithFormat:@"%@%@",[[NSString stringWithFormat:@"%@",pathToMe] stringByDeletingLastPathComponent], @"/BPM Inspector.scpt"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:checkExistence];
    if (fileExists == YES) {
        // NSLog(@"Script already exists in this location - not copying");
        return;
    }
    NSString *pathToScript = [NSString stringWithFormat:@"%@%@",pathToMe, @"/Contents/Resources/BPM Inspector.scpt"];
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] copyItemAtPath:pathToScript toPath:pathToCopyTo error:&error];
    if (success == NO) {
        NSLog(@"Copying script failed; more info below:");
        NSLog(@"%@", error);
    }
}

- (void)getOSVersion {
    NSString *versionString = [NSProcessInfo processInfo].operatingSystemVersionString;
    NSArray *versionArray = [versionString componentsSeparatedByString:@"."];
    NSString *versionBit = [versionArray objectAtIndex:1];
    OSVersion = (unsigned int)[versionBit intValue];
}

- (NSPoint)getDefaultWindowPosition {
    NSScreen *screen = [NSScreen mainScreen];
    NSRect screenRect = [screen visibleFrame];
    float x = (screenRect.size.width / 2) - 45;
    float y = (screenRect.size.height / 2) - 63;
    return NSMakePoint(x, y);
}

@end

