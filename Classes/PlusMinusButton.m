//
//  PlusMinusButton.m
//  BPM Inspector
//
//  Created by Petros Loukareas on 27-08-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "PlusMinusButton.h"


@implementation PlusMinusButton

- (void)mouseDown:(NSEvent *)event {
    buttonPosition = event.locationInWindow.x;
    [self setFirstTimer];
    [self highlight:YES];
}

- (void)mouseUp:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([repeatTimer isValid]) {
        [repeatTimer invalidate];
        [repeatTimer release];
        repeatTimer = nil;
    }
    if (buttonPosition < 30) {
        [appDelegate decreaseBPM];
    } else {
        [appDelegate increaseBPM];
    }
    [self highlight:NO];
    [appDelegate applyNewBPM];
}

- (void)setFirstTimer {
    repeatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setNextTimer) userInfo:nil repeats:NO] retain];
}

- (void)setNextTimer {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if (buttonPosition < 30) {
        [appDelegate decreaseBPM];
    } else {
        [appDelegate increaseBPM];
    }
    repeatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(setNextTimer) userInfo:nil repeats:NO] retain];
}

@end
