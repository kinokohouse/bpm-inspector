//
//  CloseButton.h
//  BPM Inspector
//
//  Created by Petros Loukareas on 27-08-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#ifndef CloseButton_h
#define CloseButton_h


#endif /* CloseButton_h */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>
#import "AppDelegate.h"


@interface CloseButton : NSButton


#pragma mark Delegate Methods

- (void)mouseEntered:(NSEvent *)event;
- (void)mouseExited:(NSEvent *)event;

@end

