//
//  CloseButton.m
//  BPM Inspector
//
//  Created by Petros Loukareas on 27-08-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#import "CloseButton.h"


@implementation CloseButton

- (void)mouseEntered:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([appDelegate OSVersion] < 10) {
        [self setImage:[NSImage imageNamed:@"classic-mouseup.png"]];
    } else {
        [self setImage:[NSImage imageNamed:@"modern-mouseup.png"]];
    }
}

- (void)mouseExited:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([appDelegate OSVersion] < 10) {
        [self setImage:[NSImage imageNamed:@"classic.png"]];
    } else {
        [self setImage:[NSImage imageNamed:@"modern.png"]];
    }
}

@end
