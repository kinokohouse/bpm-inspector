//
//  AppDelegate.h
//  BPM Inspector
//
//  Created by Petros Loukareas on 26-08-17.
//  Last updated on 25-11-17.
//  Copyright (c) 2017 Kinoko House. MIT License.
//

#ifndef AppDelegate_h
#define AppDelegate_h

#endif /* AppDelegate_h */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject {
    
    // IBOutlets
    IBOutlet NSWindow *theWindow;
    IBOutlet NSButton *theBigButton;
    IBOutlet NSImageView *leftLamp;
    IBOutlet NSImageView *rightLamp;
    IBOutlet NSTextField *bpmMeter;
    IBOutlet NSButton *closeBtn;
    IBOutlet NSImageView *fakeMinButton;
    IBOutlet NSImageView *fakeZoomButton;

    // Bookkeeping variables
    BOOL picToggle;
    double lastInterval;
    double currentAverage;
    double precisionSize;
    unsigned int maxTimeList;
    unsigned int colorIndex;
    
    // Persistent objects
    NSMutableArray *timeList;
    NSMutableArray *theColorTable;
   
    // Timers
    NSTimer *blinkTimer;
    NSTimer *delayTimer;
    
    // Fixed Images
    NSImage *regDrumImage;
    NSImage *altDrumImage;
    NSImage *lampOnImage;
    NSImage *lampOffImage;
 
    // Constants
    unsigned int OSVersion;
    
    // AppleScript
    double timeInterval;
    unsigned int beatsPerMinute;
    
}


#pragma mark Getters and Setters
- (unsigned int)beatsPerMinute;
- (void)setBeatsPerMinute:(unsigned int)value;
- (unsigned int)OSVersion;


#pragma mark IBActions

- (IBAction)gotClicked:(id)sender;
- (IBAction)gotSet:(id)sender;
- (IBAction)quitTheApp:(id)sender;


#pragma mark AppDelegate Methods

- (BOOL)application:(NSApplication *)sender delegateHandlesKey:(NSString *)key;
- (void)applicationWillFinishLaunching:(NSNotification *)notification;
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender;


#pragma mark Application Instance Methods

- (void)getDefaults;
- (void)setDefaults;
- (void)changeThePics;
- (void)changeButtonPicOnly;
- (void)increaseBPM;
- (void)decreaseBPM;
- (void)applyNewBPM;
- (void)buildColorTable;
- (void)initBlinkTimer:(NSTimeInterval)interval;
- (void)initDelayTimer:(NSTimeInterval)interval;
- (void)stopBlinkTimer;
- (void)stopDelayTimer;
- (void)fireDelayTimer;
- (void)copyResourcesIfNeeded;
- (void)getOSVersion;
- (NSPoint)getDefaultWindowPosition;

@end
